// getting loginModal
var modal = document.getElementById('loginModal');
// getting login button
var loginBtn = document.getElementById('loginBtn');


//listen for login button to open the modal
loginBtn.addEventListener('click', openLogin);

//listen for outside to close the modal
window.addEventListener('click', clickOutSide);

// function to open loginPage
function openLogin() {
    modal.style.display = 'block';
}
// function to close loginPAge
function clickOutSide(OUT) {
    if (OUT.target === modal) {
        modal.style.display = 'none';
    }

}
// hide navbar at the top of the window

window.onscroll = function () {
    if (window.scrollY > 0) {
        document.querySelector('nav').setAttribute('class', 'dark');
    } else {
        document.querySelector('nav').removeAttribute('class');
    }

};