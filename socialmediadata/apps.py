from django.apps import AppConfig


class SocialmediadataConfig(AppConfig):
    name = 'socialmediadata'
